#include <stdio.h>
#include <stdlib.h>

int main(int argc, char* argv[]) {
	
	// Katsoo onko argumentteja
	if (argc > 1) {
		// Käy argumentit läpi ja avaa tiedoston
		for(int i = 1; i < argc; i++) {
			char str[1000];
			FILE *fp = fopen(argv[i], "r");
			
			// Jos tiedostoa ei löydy
			if (fp == NULL) {
		    		printf("my-cat: cannot open file\n");
		    		exit(1);
			}
			
			// Lukee tiedoston
			while(fgets(str, 1000, fp) != NULL) {
				printf("%s",str);
			}
			// Sulkee tiedoston, lisää newlinen tiedostojen väliin
			fclose(fp);
			printf("\n");
		}
	
		return(0);
	} else {
		exit(0);
	}

	
}

