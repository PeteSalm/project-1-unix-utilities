#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char* argv[]) {
	
	// Jos käyttäjä antaa yhden tai useamman tiedoston
	if (argc > 2) {
		for(int i = 2; i < argc; i++) {
			char str[1000];
			// Avaa tiedoston lukumoodissa
			FILE *fp = fopen(argv[i], "r");
			
			// Katsoo onko tiedosto olemassa
			if (fp == NULL) {
				printf("my-grep: cannot open file\n");
				exit(1);
			}

			while(fgets(str, 1000, fp) != NULL) {
				//Katsoo onko hakuargumentti rivillä
				if (strstr(str,argv[1]) != NULL) {
					printf("%s",str);
				}
			}
			// Sulkee tiedoston ja jättää tyhjän rivin
			fclose(fp);
			printf("\n");
		}
		return(0);

	// Jos käyttäjä antaa vain hakutermin mutta ei tiedostoa
	} else if (argc == 2) {
		char str[1000];
		
		// Lukee stdin ja vertaa hakutermiä
		while(fgets(str, 1000, stdin) != NULL) {
			if (strstr(str,argv[1]) != NULL) {
				printf("%s",str);
			}
		}

		return(0);

	// Käyttäjä ei antanut mitään argumentteja
	} else {
		printf("my-grep: searchterm [file ...]\n");
		exit(1);
	}	
}

